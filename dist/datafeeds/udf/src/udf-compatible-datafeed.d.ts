import { UDFCompatibleDatafeedBase } from './udf-compatible-datafeed-base';
export declare class UDFCompatibleDatafeed extends UDFCompatibleDatafeedBase {
    constructor(datafeedURL: string, updateFrequency?: number);
}
