export const state = () => ({
  dataProviders: []
})

export const mutations = {
  SET_DATA_PROVIDERS(state, dataProviders) {
    state.dataProviders = dataProviders
  }
}

export const actions = {
  async getDataProviders({ commit, state, forceRefetch = false }) {
    let dataProviders = []
    if (state.dataProviders.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('dataproviders')
      dataProviders = response.data
      commit('SET_DATA_PROVIDERS', dataProviders)
    } else {
      dataProviders = state.dataProviders
    }
    return dataProviders
  }
}
