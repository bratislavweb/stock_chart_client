export const state = () => ({
  sectors: [],
  priorities: [],
  exchanges: [],
  categories: [],
  dataFeedNames: [],
  countries: [],
  dataProviders: []
})

export const mutations = {
  SET_SECTORS(state, sectors) {
    state.sectors = sectors
  },
  SET_PRIORITIES(state, priorities) {
    state.priorities = priorities
  },
  SET_EXCHANGES(state, exchanges) {
    state.exchanges = exchanges
  },
  SET_CATEGORIES(state, categories) {
    state.categories = categories
  },
  SET_DATA_FEED_NAMES(state, dataFeedNames) {
    state.dataFeedNames = dataFeedNames
  },
  SET_COUNTRIES(state, countries) {
    state.countries = countries
  },
  SET_DATA_PROVIDERS(state, dataProviders) {
    state.dataProviders = dataProviders
  }
}

export const actions = {
  async getSectors({ commit, state, forceRefetch = false }) {
    let sectors = []
    if (state.sectors.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('refdata/sector')
      sectors = response.data
      commit('SET_SECTORS', sectors)
    } else {
      sectors = state.sectors
    }
    return sectors
  },
  async getPriorities({ commit, state, forceRefetch = false }) {
    let priorities = []
    if (state.priorities.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('refdata/priority')
      priorities = response.data
      commit('SET_PRIORITIES', priorities)
    } else {
      priorities = state.priorities
    }
    return priorities
  },
  async getExchanges({ commit, state, forceRefetch = false }) {
    let exchanges = []
    if (state.exchanges.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('refdata/exchange')
      exchanges = response.data
      commit('SET_EXCHANGES', exchanges)
    } else {
      exchanges = state.exchanges
    }
    return exchanges
  },
  async getCategories({ commit, state, forceRefetch = false }) {
    let categories = []
    if (state.categories.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('refdata/category')
      categories = response.data
      commit('SET_CATEGORIES', categories)
    } else {
      categories = state.categories
    }
    return categories
  },
  async getDataFeedNames({ commit, state, forceRefetch = false }) {
    let dataFeedNames = []
    if (state.dataFeedNames.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('refdata/datafeedname')
      dataFeedNames = response.data
      commit('SET_DATA_FEED_NAMES', dataFeedNames)
    } else {
      dataFeedNames = state.dataFeedNames
    }
    return dataFeedNames
  },
  async getCountries({ commit, state, forceRefetch = false }) {
    let countries = []
    if (state.countries.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('refdata/country')
      countries = response.data
      commit('SET_COUNTRIES', countries)
    } else {
      countries = state.countries
    }
    return countries
  }
}
