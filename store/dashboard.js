export const state = () => ({
  dashboards: []
})

export const mutations = {
  SET_DASHBOARDS(state, dashboards) {
    state.dashboards = dashboards
  },
  ADD_DASHBOARD(state, dashboard) {
    state.dashboards.push(dashboard)
  }
}

export const actions = {
  async getDashboards({ commit, state }, forceRefetch = false) {
    let dashboards = []
    if (state.dashboards.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('dashboards')
      dashboards = response.data
      commit('SET_DASHBOARDS', dashboards)
    } else {
      dashboards = state.dashboards
    }
    return dashboards
  },
  async newDashboard(context, dashboard) {
    const response = await this.$axios.$post('dashboards', dashboard)
    const updatedDashboard = response.data
    context.commit('ADD_DASHBOARD', updatedDashboard)
  }
}
