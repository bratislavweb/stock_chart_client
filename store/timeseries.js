import _ from 'lodash'

export const state = () => ({
  timeSeriesDataCache: {
  },
  chartRefTimeSeries1: [],
  chartRefTimeSeries2: [],
  cachedAdvancedSearchFilter: {
    selectedDataProviders: [],
    selectedWatchlists: [],
    selectedDataFeedNames: [],
    selectedExchanges: [],
    selectedCountries: [],
    selectedSectors: [],
    selectedCategories: [],
    selectedPriorities: [],
    selectedTimeSeriesCodes: [],
    selectedBeforeDate: null,
    selectedBeforeDateFormatted: null,
    selectedAfterDate: null,
    selectedAfterDateFormatted: null,
    selectedTimeSeriesName: '',
    selectedTimeSeriesDescription: ''
  }
})

export const getters = {
  chartRefTimeSeries1: state => state.chartRefTimeSeries1,
  chartRefTimeSeries2: state => state.chartRefTimeSeries2,
  cachedAdvancedSearchFilter: state => state.cachedAdvancedSearchFilter
}

export const mutations = {
  ADD_TIME_SERIES_DATA_TO_CACHE(state, timeSeriesData) {
    state.timeSeriesDataCache[timeSeriesData[0].time_series_id] = timeSeriesData
  },
  SET_CHART_REF_TIME_SERIES_1(state, timeSeries) {
    state.chartRefTimeSeries1 = timeSeries
  },
  SET_CHART_REF_TIME_SERIES_2(state, timeSeries) {
    state.chartRefTimeSeries2 = timeSeries
  },
  SET_CACHED_ADVANCED_SEARCH_FILTER(state, advancedSearchFilter) {
    state.cachedAdvancedSearchFilter = _.assign(state.cachedAdvancedSearchFilter, advancedSearchFilter)
  }
}

export const actions = {
  async getTimeSeriesData(context, timeSeriesId) {
    let timeSeriesData = null
    if (!context.state.timeSeriesDataCache.hasOwnProperty(timeSeriesId) || context.state.timeSeriesDataCache[timeSeriesId] == null || context.state.timeSeriesDataCache[timeSeriesId].length === 0) {
      const response = await this.$axios.$get(`/timeseries/${timeSeriesId}/timeseriesdata?refreshOnEmpty=true`)
      timeSeriesData = response.data
      context.commit('ADD_TIME_SERIES_DATA_TO_CACHE', timeSeriesData)
    } else {
      timeSeriesData = context.state.timeSeriesDataCache[timeSeriesId]
    }
    return timeSeriesData
  }
}
