export const state = () => ({
  watchlists: []
})

export const mutations = {
  SET_WATCHLISTS(state, watchlists) {
    state.watchlists = watchlists
  },
  ADD_WATCHLIST(state, watchlist) {
    const existingIndex = state.watchlists.findIndex(function (e) {
      return e.id === watchlist.id
    })
    if (existingIndex >= 0) {
      state.watchlists.splice(existingIndex, 1, watchlist)
    } else {
      state.watchlists.push(watchlist)
    }
  }
}

export const actions = {
  async getWatchlists({ commit, state }, forceRefetch = false) {
    let watchlists = []
    if (state.watchlists.length === 0 || forceRefetch) {
      const response = await this.$axios.$get('watchlists')
      watchlists = response.data
      commit('SET_WATCHLISTS', watchlists)
    } else {
      watchlists = state.watchlists
    }
    return watchlists
  },
  async getWatchlist({ commit, state }, watchlistId) {
    let watchlist = state.watchlists.find(function (e) {
      return e.id == watchlistId
    })
    if (typeof watchlist === 'undefined') {
      const response = await this.$axios.$get(`/watchlists/${watchlistId}`)
      watchlist = response.data
      commit('ADD_WATCHLIST', watchlist)
    }
    return watchlist
  },
  async addTimeSeriesToWatchlist({ commit, state }, data) {
    if (data != null) {
      const response = await this.$axios.$post('watchlists/addTimeSeriesToWatchlists', data)
      const watchlists = response.data
      if (watchlists != null && watchlists.length > 0) {
        watchlists.forEach(function (watchlist) {
          commit('ADD_WATCHLIST', watchlist)
        })
      }
    }
  },
  async getWatchlistEntries({ commit, state }, watchlistId) {
    const response = await this.$axios.$get(`/watchlists/${watchlistId}/watchlistentries`)
    return response.data
  }
}
