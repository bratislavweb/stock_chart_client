/*
 *  This is a plugin that will be run once and is used to load up the caches and ref datas
 */
export default ({ store }) => {
  /* ref data */
  store.dispatch('refdata/getSectors')
  store.dispatch('refdata/getPriorities')
  store.dispatch('refdata/getExchanges')
  store.dispatch('refdata/getCategories')
  store.dispatch('refdata/getDataFeedNames')
  store.dispatch('refdata/getCountries')
  /* watchlists */
  store.dispatch('watchlist/getWatchlists')
  /* time series */
  store.dispatch('dataproviders/getDataProviders')
}
