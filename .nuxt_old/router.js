import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'

const _bebe657a = () => interopDefault(import('../pages/dataproviders/index.vue' /* webpackChunkName: "pages/dataproviders/index" */))
const _1a03598e = () => interopDefault(import('../pages/settings/index.vue' /* webpackChunkName: "pages/settings/index" */))
const _50faebf4 = () => interopDefault(import('../pages/sync/index.vue' /* webpackChunkName: "pages/sync/index" */))
const _607ad0a2 = () => interopDefault(import('../pages/timeseries/index.vue' /* webpackChunkName: "pages/timeseries/index" */))
const _24ff10d9 = () => interopDefault(import('../pages/timeseriesdata/index.vue' /* webpackChunkName: "pages/timeseriesdata/index" */))
const _13c1625e = () => interopDefault(import('../pages/tradingview/index.vue' /* webpackChunkName: "pages/tradingview/index" */))
const _6f74b9d1 = () => interopDefault(import('../pages/watchlists/index.vue' /* webpackChunkName: "pages/watchlists/index" */))
const _1c2743ea = () => interopDefault(import('../pages/auth/login.vue' /* webpackChunkName: "pages/auth/login" */))
const _52bb7fd2 = () => interopDefault(import('../pages/dataproviders/add.vue' /* webpackChunkName: "pages/dataproviders/add" */))
const _73361584 = () => interopDefault(import('../pages/timeseries/add.vue' /* webpackChunkName: "pages/timeseries/add" */))
const _99c59040 = () => interopDefault(import('../pages/watchlists/add.vue' /* webpackChunkName: "pages/watchlists/add" */))
const _1d11772a = () => interopDefault(import('../pages/dataproviders/_id.vue' /* webpackChunkName: "pages/dataproviders/_id" */))
const _6520b9d7 = () => interopDefault(import('../pages/timeseries/_id.vue' /* webpackChunkName: "pages/timeseries/_id" */))
const _269d7581 = () => interopDefault(import('../pages/timeseriesdata/_id.vue' /* webpackChunkName: "pages/timeseriesdata/_id" */))
const _51d8fc79 = () => interopDefault(import('../pages/watchlists/_id.vue' /* webpackChunkName: "pages/watchlists/_id" */))
const _2fb66b4c = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

if (process.client) {
  if ('scrollRestoration' in window.history) {
    window.history.scrollRestoration = 'manual'

    // reset scrollRestoration to auto when leaving page, allowing page reload
    // and back-navigation from other pages to use the browser to restore the
    // scrolling position.
    window.addEventListener('beforeunload', () => {
      window.history.scrollRestoration = 'auto'
    })

    // Setting scrollRestoration to manual again when returning to this page.
    window.addEventListener('load', () => {
      window.history.scrollRestoration = 'manual'
    })
  }
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected and scrollToTop is not explicitly disabled
  if (
    to.matched.length < 2 &&
    to.matched.every(r => r.components.default.options.scrollToTop !== false)
  ) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise((resolve) => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}

export function createRouter() {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,

    routes: [{
      path: "/dataproviders",
      component: _bebe657a,
      name: "dataproviders"
    }, {
      path: "/settings",
      component: _1a03598e,
      name: "settings"
    }, {
      path: "/sync",
      component: _50faebf4,
      name: "sync"
    }, {
      path: "/timeseries",
      component: _607ad0a2,
      name: "timeseries"
    }, {
      path: "/timeseriesdata",
      component: _24ff10d9,
      name: "timeseriesdata"
    }, {
      path: "/tradingview",
      component: _13c1625e,
      name: "tradingview"
    }, {
      path: "/watchlists",
      component: _6f74b9d1,
      name: "watchlists"
    }, {
      path: "/auth/login",
      component: _1c2743ea,
      name: "auth-login"
    }, {
      path: "/dataproviders/add",
      component: _52bb7fd2,
      name: "dataproviders-add"
    }, {
      path: "/timeseries/add",
      component: _73361584,
      name: "timeseries-add"
    }, {
      path: "/watchlists/add",
      component: _99c59040,
      name: "watchlists-add"
    }, {
      path: "/dataproviders/:id",
      component: _1d11772a,
      name: "dataproviders-id"
    }, {
      path: "/timeseries/:id",
      component: _6520b9d7,
      name: "timeseries-id"
    }, {
      path: "/timeseriesdata/:id",
      component: _269d7581,
      name: "timeseriesdata-id"
    }, {
      path: "/watchlists/:id",
      component: _51d8fc79,
      name: "watchlists-id"
    }, {
      path: "/",
      component: _2fb66b4c,
      name: "index"
    }],

    fallback: false
  })
}
