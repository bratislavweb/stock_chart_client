module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs'
  ],
  // add your custom rules here
  rules: {
    'vue/no-unused-components': 'off',
    'eqeqeq': 'off',
    'no-unused-vars': 'off',
    'new-cap': 'off'
  }
}
