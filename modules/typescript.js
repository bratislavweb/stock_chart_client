export default function () {
  // Add .ts & .tsx extension to Nuxt
  this.nuxt.options.extensions.push('ts', 'tsx')

  // Extend webpack build
  this.extendBuild(function (config) {
    // Add TypeScript
    config.module.rules.push({
      test: /\.tsx?$/,
      loader: 'ts-loader',
      options: { 
        appendTsSuffixTo: [/\.vue$/],
        "compilerOptions": {
          "target": "es5",
          "lib": [
            "dom",
            "es2015"
          ],
          "module": "es2015",
          "moduleResolution": "node",
          "experimentalDecorators": true,
          "declaration": true,
          "noImplicitAny": false,
          "noImplicitThis": false,
          "strictNullChecks": true,
          "removeComments": true,
          "suppressImplicitAnyIndexErrors": true,
          "allowSyntheticDefaultImports": true,
          "baseUrl": ".",
          "paths": {
            "~": ["./"],
            "~assets/*": ["./assets/*"],
            "~components/*": ["./components/*"],
            "~middleware/*": ["./middleware/*"],
            "~pages/*": ["./pages/*"],
            "~plugins/*": ["./plugins/*"],
            "~static/*": ["./static/*"],
            "~store": ["./.nuxt/store"],
            "~router": ["./.nuxt/router"]
          }
        }
      }
    })

    // Add .ts extension in webpack resolve
    if (!config.resolve.extensions.includes('.ts')) {
      config.resolve.extensions.push('.ts')
    }

    // Add .tsx extension in webpack resolve
    if (!config.resolve.extensions.includes('.tsx')) {
      config.resolve.extensions.push('.tsx')
    }
  })
}